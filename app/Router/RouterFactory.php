<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
    use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
        $router = new RouteList;
        /**
         * profile
         */
        $router->addRoute(
            'admin/<presenter>/<action>',
            ['module' => 'Admin', 'presenter' => 'Dashboard', 'action' => 'default']
        );
        $router->addRoute(
            'user/<presenter>/<action>',
            ['module' => 'User', 'presenter' => 'Homepage', 'action' => 'default']
        );
        $router->addRoute(
            '<presenter>/<action>',
            ['module' => 'Front', 'presenter' => 'Homepage', 'action' => 'default']
        );
        return $router;
    }
}
