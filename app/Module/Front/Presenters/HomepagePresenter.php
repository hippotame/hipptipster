<?php

declare(strict_types=1);

namespace App\Module\Front\Presenters;

/**
 * @author Tomáš Filip <dev@tomas-filip.com>
 * @since 1.0.0
 */
class HomepagePresenter extends BasePresenter
{
}
