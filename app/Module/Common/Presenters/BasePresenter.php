<?php

declare(strict_types=1);
namespace App\Module\Common\Presenters;

use Nette;

/**
 * @author Tomáš Filip <dev@tomas-filip.com>
 * @since 1.0.0
 */
class BasePresenter extends Nette\Application\UI\Presenter
{
}
