<?php

declare(strict_types=1);

namespace App\Module\Admin\Presenters;

final class DashboardPresenter extends \App\Module\Common\Presenters\CommonPresenter
{
    use AdminRequireLoggedUser;
}
